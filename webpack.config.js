const path = require('path');

module.exports = {
  mode: "development",
  entry: "./src/app",
  devtool: "inline-source-map",
  target: "node",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "index.[chunkhash].js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    ]
  }
  
}