import express from 'express'
import Demo from './demo'

const router = express.Router()

router.use('/demo', Demo)

export default router
