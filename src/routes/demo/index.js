import express from 'express'

const router = express.Router();

router.get('/get', function(req, res, next) {
  res.send({ demo : 'hello express' })
});

router.post('/post', function(req, res, next) {
  res.send({ demo : 'hello demo', data : req.body })
});

export default router
