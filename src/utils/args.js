const args = process.argv
const result = {}

args.forEach(item=>{
    let key, value
    if(item.startsWith('--')){
        key = item.substr(2, item.indexOf('=') - 2)
        value = item.substr(item.indexOf('=') + 1)
        result[key] = value
    }
})

export default result