import express from 'express'
import logger from 'morgan'
import routers from './routes'
import args from './utils/args'

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/api', routers);

app.listen(args['server.port'] || 3000, () => {
    console.log(`server running http://localhost:${args['server.port'] || 3000}`);
});